"""
Created By: Lilly (lycho13@student.monash.edu)

"""

import sys
sys.path.append('../UTTT')
from State import State, get_possible_next_moves, pretty_print

import copy
import unittest
import random
import math

import new_settings

from numpy.random import choice

def new_score(node):
    base_score = node.win_count/(node.total_simulations * 1.0) #force floating point division
    return base_score

class NewNode():
    def __init__(self, state):
        self.state = state
        self.parent = None
        self.children = [] #list of children nodes

        self.win_count = 0
        self.total_simulations = 0

        self.score = float("inf")

    def __str__(self, level=0):
        string = "\t"*level + repr(self).replace("\n", "\n" + "\t"*level) + "\n"
        for child in self.children:
            string += child.__str__(level+1)
        return string

    def __repr__(self):
        string = str(self.win_count) + "/" + str(self.total_simulations)
        return str(string)

    def add_child(self, child):
        #creates the association between parent and child nodes
        self.children.append(child)
        child.parent = self

    def best_child(self):
        children_excl = [c for c in self.children if c.score != float("inf")] #exclude children not "fully created"
        children_excl.sort(key=lambda x: x.score, reverse=True) #sort by attribute
        best_child = children_excl[0]
        return best_child

    def new_best_child(self):
        ALPHA = new_settings.ALPHA
        
        self.children.sort(key=lambda x: x.score, reverse=True) #sort by attribute
        if self.children[0].score == float("inf"): #if not "fully created" child exists, it is always returned first
            return self.children[0]
        
        #else
        weights = [ALPHA*math.exp(-ALPHA*i) for i in list(range(len(self.children)))]

        p_factor = 1/sum(weights)
        normalized_weights = [w*p_factor for w in weights]

        new_best_child = choice(self.children, p=normalized_weights)
        return new_best_child

    def next_child(self):        
        if self.children == []: #if leaf node
            #create all child nodes
            possible_next_moves = get_possible_next_moves(self.state)
            #print(possible_next_moves)
            for move in possible_next_moves:
                child = NewNode(copy.deepcopy(self.state))
                child.state.move(move[0], move[1])
                self.add_child(child)
                
            if self.total_simulations == 0: #only happens at root node
                return self
            
            if len(possible_next_moves) == 0: #if tree is built to last state, i.e. no more moves
                return self
            else:    
                return self.children[random.randint(0, len(self.children)-1)] #else: return random child
            
        else:
            #recursive action
            best_child = self.new_best_child()

            if best_child.score == float("inf"): #if child left to be added
                return best_child
            else:
                return best_child.next_child()

        
class TestNode(unittest.TestCase):
    def test_init(self):
        s = State([0]*9, 0, 1)
        n = NewNode(s)
        self.assertEqual(n.state, s)
        self.assertIsNone(n.parent)
        self.assertEqual(n.children, [])
        self.assertEqual(n.win_count, 0)
        self.assertEqual(n.total_simulations, 0)

    def test_add_child(self):
        s = State([0]*9, 0, 1)
        n = NewNode(s)
        self.assertEqual(n.children, [])

        c = NewNode(s)
        n.add_child(c)
        self.assertEqual(n.children, [c])
        self.assertEqual(c.parent, n)
        d = NewNode(s)
        n.add_child(d)
        self.assertEqual(n.children, [c, d])
        self.assertEqual(c.parent, n)
        self.assertEqual(d.parent, n)

    def test_best_child(self):
        s = State([0]*9, 0, 1)
        n = NewNode(s)
        c1 = NewNode(s)
        c2 = NewNode(s)
        c3 = NewNode(s)

        n.add_child(c1)
        n.add_child(c2)
        n.add_child(c3)

        c2.score = 1
        c3.score = 2

        self.assertEqual(n.best_child(), c3)

    def test_new_best_child(self):
        s = State([0]*9, 0, 1)
        n = NewNode(s)
        c1 = NewNode(s)
        c2 = NewNode(s)
        c3 = NewNode(s)

        n.add_child(c1)
        n.add_child(c2)
        n.add_child(c3)

        c1.score = 1
        c2.score = 100

        self.assertEqual(n.new_best_child(), c3)

    def test_next_child(self):
        s = State([[0]*9 for i in range(9)], None, 1) #root state
        n = NewNode(s) #root node
        self.assertEqual(n.next_child(), n)

        n.total_simulations = 1
        random_node = n.next_child()
        self.assertIn(random_node, n.children)
        

if __name__ == "__main__":
    unittest.main()
