BIAS = 0.1
GRAVE win count/total non-draw games = 93/100
BIAS = 0.01
GRAVE win count/total non-draw games = 96/100
BIAS = 0.001
GRAVE win count/total non-draw games = 89/100
BIAS = 0.0001
GRAVE win count/total non-draw games = 85/98
BIAS = 1e-05
GRAVE win count/total non-draw games = 90/98
BIAS = 1e-06
GRAVE win count/total non-draw games = 86/98
BIAS = 1e-07
GRAVE win count/total non-draw games = 90/100
BIAS = 1e-08
GRAVE win count/total non-draw games = 89/100
BIAS = 1e-09
GRAVE win count/total non-draw games = 88/99
BIAS = 1e-10
GRAVE win count/total non-draw games = 92/100
BIAS = 1e-11
GRAVE win count/total non-draw games = 87/100
BIAS = 1e-12
GRAVE win count/total non-draw games = 91/100
BIAS = 1e-13
GRAVE win count/total non-draw games = 92/100
BIAS = 1e-14
GRAVE win count/total non-draw games = 92/99
BIAS = 1e-15
GRAVE win count/total non-draw games = 95/98
