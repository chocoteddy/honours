# Welcome to Lilly's Honour's Project!✨ #
゜・。・゜・While you're here, please feel free to have a cookie -> 🍪

## 🌼 An overview ##
 * __MCTS(Vanilla)__ is my original, first-mint implementation of basic Monte Carlo Tree Search. Nothing interesting here. Exists solely as a fallback if I ever get lost throughly enough in my own code.
 * __UCT__ is an implementation of Upper Confidence Bound for Tree i.e. UCT-MCTS. It is the most commonly "benchmarked against" variant of MCTS.
 * __DECISIVE__ is an implementation of Decisive-MCTS. It is the algorithm which has shown the most promising potential in sacrifice scenarios.
 * __GRAVE__ is an implementation of Generalized Rapid Action Value Estimation-MCTS. It is one of the more relevant/recent MCTS variant available (published 2015).
 * __NEW__ is an implementation of our proposed improvement, few-best selection-MCTS. It selects a node using exponentially diminishing probabilities which correlate to node values.
 * __UTTT__ defines Ultimate Tic-tac-toe game states and rules.
 * __EXPERIMENTS__ are script setups for running proposed tests and experiments. Results are stored in a subfolder __OUTPUT__ as plain text files (.txt).

## 🦄 Definitions ##
* Trees in MCTS are not DAGs.
* In each iteration of MCTS, _exactly one_ simulation is played and _exactly one_ node is created.

![MCTS iterations](https://docs.google.com/uc?id=0B-Vq2G3EGHHGeXhteFA0a2VmTm8)

* At each __move/turn__ of a given __game/trial__ in UTTT, both players have the same time/__iteration__ rounds to "grow" their tree, but one must play a move by the end of the turn.

> P/s: Pay special attention to the __bolded words__ in the definitions above. They will be important in understanding the experiments later on.

## 🍰 Slicing the works ##
Each node stores a `win_count`, `total_simulations` and `score`. Implementation-wise, each node also stores a list of `children` and a reference to its `parent`.

1. Selection
    * Recurse until best scored, leaf node
    * If "ghost child" present, return it.
    * Else If children left to be added, create all children as "ghosts". Then return any "ghost child".
    ![children of the ghosts](https://docs.google.com/uc?id=0B-Vq2G3EGHHGc1Z0Vlp4QkpkWU0)
    > _What am I doing here?_ - The expansion step that follows this uses a sub-function that returns every possible move from a given state.  
    > Consider that __every time__ we want to create a child, we need to do an every pair, parent-child comparison to find which child has and hasn't been created.  
    > As this is expensive, we are instead creating all the children at once and simply having the selection function treat them as if they aren't really there yet. Hence the "ghost child" name.

2. Expansion
    * If "ghost child", convert to plain child node.

3. Simulation
    * Simulate to end game. Report win/lose.

4. Backpropagation
    * Update tree using simulation results.
    
## 👾 How to play ##
Want to join the fun? Start by cloning the repository. Then, edit file UTTT/UTTT.py to choose your preferred implementation to fight out.
Change players `agent_x` and `agent_o` to one of `Uct()`, `Decisive()`, `Grave()` and `New()`. Always use the argument `1` for `agent_x` and `-1` for `agent_o`. 

> _Why 1 and -1?_ - The evaluation function that determines if a state is a win state or otherwise depends on the player's markers being these numbers to work.  
> See the method `inner_win()` in UTTT/State.py to understand the magic. 

* For example, to play UCT (as player X, starts first) against GRAVE (as player O): from line 28, in UTTT/UTTT.py

        self.agent_x = Uct(1)
        self.agent_o = Grave(-1)
        
To change the number of iterations at each turn, simply modify the for loop condition which surrounds the `iterate()` function calls.
        
* For example, to give each player 250 iterations "to think" at each turn, from line 37, in UTTT/UTTT.py

        for i in range(250):
    
Once you have set your agents, run UTTT/UTTT.py and give it a minute a two! Results will be printed at the end of the game, along with relevant output information.
If the winner is `1`, X has won. If the winner is `-1`, O has won. If the winner is `0`, then the game has ended in a draw.

* Example output of a game with New(1) vs. Grave(-1)

        winner: -1
                      │               │              
            │ O │ O   │     │   │     │   O │ X │ O
         ───┼───┼───  │  ───┼───┼───  │  ───┼───┼───
          X │ O │ O   │     │   │     │   X │ O │ X
         ───┼───┼───  │  ───┼───┼───  │  ───┼───┼───
          X │ O │ X   │   X │ X │ X   │   X │ O │ X
        ──────────────┼───────────────┼──────────────
            │   │ X   │     │ O │     │     │ O │  
         ───┼───┼───  │  ───┼───┼───  │  ───┼───┼───
          O │ X │ O   │   X │ O │     │     │ X │  
         ───┼───┼───  │  ───┼───┼───  │  ───┼───┼───
          X │ O │ X   │   X │ O │ O   │   X │ X │ X
        ──────────────┼───────────────┼──────────────
            │   │     │   X │ O │ X   │     │   │ O
         ───┼───┼───  │  ───┼───┼───  │  ───┼───┼───
          O │ O │ O   │   O │ O │ X   │     │ X │ O
         ───┼───┼───  │  ───┼───┼───  │  ───┼───┼───
          X │   │     │   O │   │ X   │   O │ X │ O
                      │               │              
        time taken: 33.05485725402832
      
To change an algorithm's parameter values, such as the _c_ value in UCT, the _bias_ and _ref_ values in GRAVE or even the _alpha_ value in New, 
simply access the corresponding <MCTS_variant>/<mcts_variant>_settings.py file and edit the values within.
    
## 👓 Experiments ##
_These tables will be filled eventually. . ._

### Tuning Parameters ###

Tuning the _bias_ in GRAVE against UCT at UTTT with 100 trials, 100 iterations each turn:

| _bias_          | 10⁻¹  | 10⁻²  | 10⁻³  | 10⁻⁴  | 10⁻⁵  | 10⁻⁶  | 10⁻⁷  | 10⁻⁸  | 10⁻⁹  | 10⁻¹⁰ | 10⁻¹¹ | 10⁻¹² | 10⁻¹³ | 10⁻¹⁴ | __10⁻¹⁵__ |
|:--------------- | :---: | :---: | ----: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :-------: |
| GRAVE win rate  | 93.0% | 96.0% | 89.0% | 86.7% | 91.8% | 87.8% | 90.0% | 89.0% | 88.9% | 92.0% | 87.0% | 91.0% | 92.0% | 92.9% | __96.9%__ |

> _What ref value are we using to tune the bias?_ - GRAVE with a _ref_ value of `0` is equivalent to a standard implementation of RAVE and is the value which will be used. 

Tuning the _ref_ in GRAVE against UCT at UTTT with 100 trials, 100 iterations each turn:

| _ref_           | 1     | 5     | __10__    | 20    | 25    | 50    | 100   | 200   | 400   |
|:--------------- | :---: | :---: | :-------: | :---: | :---: | :---: | :---: | :---: | :---: |
| GRAVE win rate  | 85.7% | 88.0% | __96.0%__ | 92.9% | 89.0% | 92.0% | 82.7% | 82.7% | 78.4% |

> _Why these values?_ - These were the original tuning tests ran in the paper which first proposed GRAVE; except while in the paper the algorithm was used to play Atarigo/Go/Domineering etc., here we are playing UTTT.

> _What about the c value?_ - A value of `0.4` will be used, which is the recommended value used in GGP.

Tuning the _alpha_ in New against UCT at UTTT in 100 trials, 100 iterations each turn:

|_alpha_       | __0.05__  | 0.10  | 0.15  | 0.20  | 0.25  | 0.30  | 0.35  | 0.40  | 0.45  | 0.50  | 0.55  | 0.60  | 0.65  | 0.70  | 0.75  | 0.80  | 0.85  | 0.90  | 0.95  | 
|:------------ | :-------: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
|New win rate  | __58.9%__ | 47.4% | 52.0% | 51.6% | 41.2% | 54.6% | 48.5% | 45.3% | 52.1% | 42.3% | 51.5% | 57.7% | 54.2% | 55.1% | 53.5% | 48.0% | 50.5% | 46.7% | 40.6% |

> _Why these values?_ - The original paper had little detail on how this parameter was tuned, but claimed that values `0.05` and `0.25` produced pseudo-optimal solutions.  
> Leaving me open for imagination (the value for alpha must be between 0<α<1), I simply figured that testing all possible values in an interval of 0.05 should be sufficient for our purposes.

### General Playing Strength ###

Round robin (read row first i.e. row as first player, row as winner) in 100 trials, 100 iterations each turn:

| Agent         | UCT       | New       | Decisive  | GRAVE     |
|:------------- | :-------: | :-------: | :-------: | :-------: |
| __UCT__       |     -     |   60.2%   |   48.5%   |   11.1%   |
| __New__       |   59.6%   |     -     |   57.7%   |   12.1%   |
| __Decisive__  |   58.2%   |   49.5%   |     -     |   13.0%   |
| __GRAVE__     |   96.0%   |   94.0%   |   94.9%   |     -     |

### Sacrifice-play Strength ###

__All setups will be tested in 100 trials.__ Scores show *rate of sacrifice path play*__/__*rate of sacrifice move play*__.__ 

Rate of sacrifice play in a terminal, 3-move scenario:

| Agent\Iterations  | 100     | 1000    | 10 000  |
|:----------------- | :-----: | :-----: | :-----: |
| __UCT__           |   4/4   |   2/2   |   1/1   |
| __New__           |   0/58  |   0/58  |   0/57  |
| __Decisive__      |  17/17  |  15/15  |   7/7   |
| __GRAVE__         |  18/19  |  28/28  |  29/29  |

Rate of sacrifice play in a terminal, 5-move scenario:

| Agent\Iterations  | 100     | 1000    | 10 000  |
|:----------------- | :-----: | :-----: | :-----: |
| __UCT__           |   0/3   |   0/1   |   1/3   |
| __New__           |   0/27  |   0/21  |   0/26  |
| __Decisive__      |   1/1   |   1/2   |   4/4   |
| __GRAVE__         |   7/10  |  12/12  |   8/10  |

Rate of sacrifice play in a terminal, 7-move scenario:

| Agent\Iterations  | 100     | 1000    | 10 000  |
|:----------------- | :-----: | :-----: | :-----: |
| __UCT__           |   0/1   |   0/1   |   0/3   |
| __New__           |   0/19  |   0/26  |   0/36  |
| __Decisive__      |   0/9   |   0/6   |   1/3   |
| __GRAVE__         |  10/13  |  58/60  |  62/63  |

Rate of sacrifice play in a non-terminal, 3-move scenario:

| Agent\Iterations  | 100     | 1000    | 10 000  |
|:----------------- | :-----: | :-----: | :-----: |
| __UCT__           |   0/3   |   0/0   |   0/5   |
| __New__           |   1/28  |   2/20  |   2/16  |
| __Decisive__      |   0/1   |   0/1   |   0/6   |
| __GRAVE__         |   7/7   |  16/17  |  27/27  |

Rate of sacrifice play in a non-terminal, 5-move scenario:

| Agent\Iterations  | 100     | 1000    | 10 000  |
|:----------------- | :-----: | :-----: | :-----: |
| __UCT__           |   0/0   |   0/0   |   1/8   |
| __New__           |   0/17  |   1/16  |   0/21  |
| __Decisive__      |   1/2   |   1/1   |   4/6   |
| __GRAVE__         |   0/13  |   3/14  |   0/13  |

Rate of sacrifice play in a non-terminal, 7-move scenario:

| Agent\Iterations  | 100     | 1000    | 10 000  |
|:----------------- | :-----: | :-----: | :-----: |
| __UCT__           |   0/0   |   0/2   |   0/10  |
| __New__           |   0/10  |   0/13  |   0/9   |
| __Decisive__      |   0/1   |   0/7   |   0/6   |
| __GRAVE__         |   2/11  |  22/23  |  57/57  |
