Using UCT with terminal 7-turn scenario, 100 iterations in 100 trials
Sacrifice path play/sacrifice move play = 4/24
Using UCT with terminal 7-turn scenario, 1000 iterations in 100 trials
Sacrifice path play/sacrifice move play = 5/22
Using UCT with terminal 7-turn scenario, 10000 iterations in 100 trials
Sacrifice path play/sacrifice move play = 3/22
Using Decisive with terminal 7-turn scenario, 100 iterations in 100 trials
Sacrifice path play/sacrifice move play = 4/39
Using Decisive with terminal 7-turn scenario, 1000 iterations in 100 trials
Sacrifice path play/sacrifice move play = 1/31
Using Decisive with terminal 7-turn scenario, 10000 iterations in 100 trials
Sacrifice path play/sacrifice move play = 5/38
Using GRAVE with terminal 7-turn scenario, 100 iterations in 100 trials
Sacrifice path play/sacrifice move play = 0/20
Using GRAVE with terminal 7-turn scenario, 1000 iterations in 100 trials
Sacrifice path play/sacrifice move play = 1/15
Using GRAVE with terminal 7-turn scenario, 10000 iterations in 100 trials
Sacrifice path play/sacrifice move play = 1/13
Using New with terminal 7-turn scenario, 100 iterations in 100 trials
Sacrifice path play/sacrifice move play = 2/12
Using New with terminal 7-turn scenario, 1000 iterations in 100 trials
Sacrifice path play/sacrifice move play = 0/2
Using New with terminal 7-turn scenario, 10000 iterations in 100 trials
Sacrifice path play/sacrifice move play = 0/2
