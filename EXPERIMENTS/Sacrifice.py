"""
Created By: Lilly (lycho13@student.monash.edu)

-Scripts to run sacrifice tests.

"""

import time

import sys
sys.path.append('../UCT')
sys.path.append('../DECISIVE')
sys.path.append('../GRAVE')
sys.path.append('../NEW')
sys.path.append('../UTTT')

from Uct import Uct
from Decisive import Decisive
from Grave import Grave
from New import New

from State import State, move_from_states, pretty_print

from Play import play_sacrifice

import copy


def mcts_variant(variant_name, player_token):
    variant_name = variant_name.capitalize()
    return globals()[variant_name](player_token)

def terminal(A, B, turns, iterations, sacrifice_state, last_move, sacrifice_sequence, output_file):
    sacrifice_move_play_count = 0
    sacrifice_path_play_count = 0
    with open(output_file, "a") as results:
        for i in range(100):
            agent_x = mcts_variant(A, 1)
            agent_o = mcts_variant(B, -1)
            moves_history = play_sacrifice(sacrifice_state, last_move, agent_x, agent_o, turns, iterations)
            #print(moves_history)
            #print(pretty_print(agent_x.root.state))
            if moves_history[0] == sacrifice_sequence[0]:
                sacrifice_move_play_count += 1
            if moves_history == sacrifice_sequence:
                sacrifice_path_play_count += 1
        results.write("Using {0} with terminal {1}-turn scenario, {2} iterations in 100 trials\nSacrifice path play/sacrifice move play = {3}/{4}\n".format(A,
                                                                                                                                                            turns,
                                                                                                                                                            iterations,
                                                                                                                                                            sacrifice_path_play_count,
                                                                                                                                                            sacrifice_move_play_count))
    

def non_terminal(A, B, turns, iterations, sacrifice_state, last_move, sacrifice_sequence, output_file):
    sacrifice_move_play_count = 0
    sacrifice_path_play_count = 0
    with open(output_file, "a") as results:
        for i in range(100):
            agent_x = mcts_variant(A, 1)
            agent_o = mcts_variant(B, -1)
            moves_history = play_sacrifice(sacrifice_state, last_move, agent_x, agent_o, turns, iterations)
            #print(moves_history)
            #print(pretty_print(agent_x.root.state))
            if moves_history[0] == sacrifice_sequence[0]:
                sacrifice_move_play_count += 1
            if moves_history == sacrifice_sequence:
                sacrifice_path_play_count += 1
        results.write("Using {0} with non-terminal {1}-turn scenario, {2} iterations in 100 trials\nSacrifice path play/sacrifice move play = {3}/{4}\n".format(A,
                                                                                                                                                                turns,
                                                                                                                                                                iterations,
                                                                                                                                                                sacrifice_path_play_count,
                                                                                                                                                                sacrifice_move_play_count))
    

if __name__ == "__main__":
##    #for terminal 3
    sacrifice_state = [[0, 0, 0, 1, 1, 1, 0, 0, 0],
                       [0, 0, 0, 1, 1, 1, 0, 0, 0],
                       [0, 0, 0, 1, 1, 0, 0, 0, 0],
                       [-1, -1, -1, 0, 0, 0, 0, 0, 0],
                       [-1, -1, -1, 0, 0, 0, 0, 0, 0],
                       [-1, -1, 0, 0, 0, 0, 0, 0, 0],
                       [0, 0, 0, 0, 0, 0, 0, 0, 0],
                       [0, 0, 0, 0, 0, 0, 0, 0, 0],
                       [1, 1, -1, 1, 1, -1, -1, -1, 0]]
    #pretty_print(State(sacrifice_state, None, 1))
    sacrifice_sequence = [[6,8], [8,8], [2,5]]
    last_move = 6
    terminal("UCT", "UCT", 3, 100, sacrifice_state, last_move, sacrifice_sequence, "OUTPUT/Terminal-3.txt")
    terminal("New", "UCT", 3, 100, sacrifice_state, last_move, sacrifice_sequence, "OUTPUT/Terminal-3.txt")
    terminal("Decisive", "UCT", 3, 100, sacrifice_state, last_move, sacrifice_sequence, "OUTPUT/Terminal-3.txt")
    terminal("GRAVE", "UCT", 3, 100, sacrifice_state, last_move, sacrifice_sequence, "OUTPUT/Terminal-3.txt")
    
    terminal("UCT", "UCT", 3, 1000, sacrifice_state, last_move, sacrifice_sequence, "OUTPUT/Terminal-3.txt")
    terminal("New", "UCT", 3, 1000, sacrifice_state, last_move, sacrifice_sequence, "OUTPUT/Terminal-3.txt")
    terminal("Decisive", "UCT", 3, 1000, sacrifice_state, last_move, sacrifice_sequence, "OUTPUT/Terminal-3.txt")
    terminal("GRAVE", "UCT", 3, 1000, sacrifice_state, last_move, sacrifice_sequence, "OUTPUT/Terminal-3.txt")
    
    terminal("UCT", "UCT", 3, 10000, sacrifice_state, last_move, sacrifice_sequence, "OUTPUT/Terminal-3.txt")
    terminal("New", "UCT", 3, 10000, sacrifice_state, last_move, sacrifice_sequence, "OUTPUT/Terminal-3.txt")
    terminal("Decisive", "UCT", 3, 10000, sacrifice_state, last_move, sacrifice_sequence, "OUTPUT/Terminal-3.txt")
    terminal("GRAVE", "UCT", 3, 10000, sacrifice_state, last_move, sacrifice_sequence, "OUTPUT/Terminal-3.txt")

##    #for terminal 5
    sacrifice_state = [[1, 1, 1, 0, 0, 0, 0, 0, 0],
                       [1, 1, 1, 0, 0, 0, 0, 0, 0],
                       [1, 1, 0, 0, 0, -1, 0, 0, -1],
                       [0, 0, 0, 0, 0, 0, 0, 0, 0],
                       [-1, -1, 0, 1, 1, -1, 1, 1, -1],
                       [0, 0, -1, 0, 0, -1, 0, 0, -1],
                       [0, 0, 0, 0, 1, 0, 0, 0, 0],
                       [-1, -1, 1, 0, 1, -1, -1, 1, -1],
                       [0, 0, -1, 0, 0, -1, 0, 0, -1]]
    sacrifice_sequence = [[6,7], [7,3], [3,4], [4,2], [2,2]]
    last_move = 6
    terminal("UCT", "UCT", 5, 100, sacrifice_state, last_move, sacrifice_sequence, "OUTPUT/Terminal-5.txt")
    terminal("New", "UCT", 5, 100, sacrifice_state, last_move, sacrifice_sequence, "OUTPUT/Terminal-5.txt")
    terminal("Decisive", "UCT", 5, 100, sacrifice_state, last_move, sacrifice_sequence, "OUTPUT/Terminal-5.txt")
    terminal("GRAVE", "UCT", 5, 100, sacrifice_state, last_move, sacrifice_sequence, "OUTPUT/Terminal-5.txt")

    terminal("UCT", "UCT", 5, 1000, sacrifice_state, last_move, sacrifice_sequence, "OUTPUT/Terminal-5.txt")
    terminal("New", "UCT", 5, 1000, sacrifice_state, last_move, sacrifice_sequence, "OUTPUT/Terminal-5.txt")
    terminal("Decisive", "UCT", 5, 1000, sacrifice_state, last_move, sacrifice_sequence, "OUTPUT/Terminal-5.txt")
    terminal("GRAVE", "UCT", 5, 1000, sacrifice_state, last_move, sacrifice_sequence, "OUTPUT/Terminal-5.txt")

    terminal("UCT", "UCT", 5, 10000, sacrifice_state, last_move, sacrifice_sequence, "OUTPUT/Terminal-5.txt")
    terminal("New", "UCT", 5, 10000, sacrifice_state, last_move, sacrifice_sequence, "OUTPUT/Terminal-5.txt")
    terminal("Decisive", "UCT", 5, 10000, sacrifice_state, last_move, sacrifice_sequence, "OUTPUT/Terminal-5.txt")
    terminal("GRAVE", "UCT", 5, 10000, sacrifice_state, last_move, sacrifice_sequence, "OUTPUT/Terminal-5.txt")

##    #for terminal 7
    sacrifice_state = [[1, 0, 0, 0, 0, 0, 1, 0, 0],
                       [0, 1, 0, 0, 0, 0, 0, 1, 0],
                       [1, 1, 0, 0, 0, -1, 0, 0, -1],
                       [1, 0, -1, -1, 1, 1, -1, 1, -1],
                       [-1, -1, 0, 1, 1, -1, 1, 1, -1],
                       [0, 0, -1, 0, 0, -1, 0, 0, -1],
                       [0, 0, 0, 1, 1, 0, 0, 0, 0],
                       [0, 1, -1, 1, -1, -1, 1, -1, 1],
                       [0, 0, -1, 0, 0, -1, 0, 0, -1]]
    sacrifice_sequence = [[6,7], [7,0], [0,3], [3,1], [1,4], [4,2], [2,2]]
    last_move = 6
    terminal("UCT", "UCT", 7, 100, sacrifice_state, last_move, sacrifice_sequence, "OUTPUT/Terminal-7.txt")
    terminal("New", "UCT", 7, 100, sacrifice_state, last_move, sacrifice_sequence, "OUTPUT/Terminal-7.txt")
    terminal("Decisive", "UCT", 7, 100, sacrifice_state, last_move, sacrifice_sequence, "OUTPUT/Terminal-7.txt")
    terminal("GRAVE", "UCT", 7, 100, sacrifice_state, last_move, sacrifice_sequence, "OUTPUT/Terminal-7.txt")

    terminal("UCT", "UCT", 7, 1000, sacrifice_state, last_move, sacrifice_sequence, "OUTPUT/Terminal-7.txt")
    terminal("New", "UCT", 7, 1000, sacrifice_state, last_move, sacrifice_sequence, "OUTPUT/Terminal-7.txt")
    terminal("Decisive", "UCT", 7, 1000, sacrifice_state, last_move, sacrifice_sequence, "OUTPUT/Terminal-7.txt")
    terminal("GRAVE", "UCT", 7, 1000, sacrifice_state, last_move, sacrifice_sequence, "OUTPUT/Terminal-7.txt")

    terminal("UCT", "UCT", 7, 10000, sacrifice_state, last_move, sacrifice_sequence, "OUTPUT/Terminal-7.txt")
    terminal("New", "UCT", 7, 10000, sacrifice_state, last_move, sacrifice_sequence, "OUTPUT/Terminal-7.txt")
    terminal("Decisive", "UCT", 7, 10000, sacrifice_state, last_move, sacrifice_sequence, "OUTPUT/Terminal-7.txt")
    terminal("GRAVE", "UCT", 7, 10000, sacrifice_state, last_move, sacrifice_sequence, "OUTPUT/Terminal-7.txt")

##    #for non-terminal 3
    sacrifice_state = [[-1, 0, 0, 0, -1, 0, 0, 0, -1],
                       [0, 0, 0, 0, 0, 0, 0, 0, 0],
                       [0, 0, 1, 0, 1, 0, 1, 0, 0],
                       [0, 0, 0, 0, 0, 0, 0, 0, 0],
                       [-1, -1, 0, 0, 0, 1, 0, 0, 1],
                       [0, 0, 0, 0, 0, 0, 0, 0, 0],
                       [0, 0, 0, 0, 0, 0, 0, 0, 0],
                       [-1, -1, 1, 1, 0, -1, 1, -1, 1],
                       [0, 0, 0, 0, 0, 0, 0, 0, 0]]
    sacrifice_sequence = [[6,7], [7,4], [4,2]]
    last_move = 6
    non_terminal("UCT", "UCT", 3, 100, sacrifice_state, last_move, sacrifice_sequence, "OUTPUT/Non-Terminal-3.txt")
    non_terminal("New", "UCT", 3, 100, sacrifice_state, last_move, sacrifice_sequence, "OUTPUT/Non-Terminal-3.txt")
    non_terminal("Decisive", "UCT", 3, 100, sacrifice_state, last_move, sacrifice_sequence, "OUTPUT/Non-Terminal-3.txt")
    non_terminal("GRAVE", "UCT", 3, 100, sacrifice_state, last_move, sacrifice_sequence, "OUTPUT/Non-Terminal-3.txt")

    non_terminal("UCT", "UCT", 3, 1000, sacrifice_state, last_move, sacrifice_sequence, "OUTPUT/Non-Terminal-3.txt")
    non_terminal("New", "UCT", 3, 1000, sacrifice_state, last_move, sacrifice_sequence, "OUTPUT/Non-Terminal-3.txt")
    non_terminal("Decisive", "UCT", 3, 1000, sacrifice_state, last_move, sacrifice_sequence, "OUTPUT/Non-Terminal-3.txt")
    non_terminal("GRAVE", "UCT", 3, 1000, sacrifice_state, last_move, sacrifice_sequence, "OUTPUT/Non-Terminal-3.txt")

    non_terminal("UCT", "UCT", 3, 10000, sacrifice_state, last_move, sacrifice_sequence, "OUTPUT/Non-Terminal-3.txt")
    non_terminal("New", "UCT", 3, 10000, sacrifice_state, last_move, sacrifice_sequence, "OUTPUT/Non-Terminal-3.txt")
    non_terminal("Decisive", "UCT", 3, 10000, sacrifice_state, last_move, sacrifice_sequence, "OUTPUT/Non-Terminal-3.txt")
    non_terminal("GRAVE", "UCT", 3, 10000, sacrifice_state, last_move, sacrifice_sequence, "OUTPUT/Non-Terminal-3.txt")

##    #for non-terminal 5
    sacrifice_state = [[0, 0, 0, 0, 0, 0, 0, 0, 0],
                       [1, -1, 1, -1, 0, 1, -1, 1, -1],
                       [0, 0, 0, 0, 0, 0, 0, 0, 0],
                       [0, 0, 0, 0, 0, 0, 0, 0, 0],
                       [1, 0, 0, 0, -1, -1, 1, -1, -1],
                       [0, 0, 0, 0, 0, 0, 0, 0, 0],
                       [0, 1, 0, 0, 0, 0, 0, 0, 0],
                       [0, -1, -1, -1, 1, 1, -1, 1, 1],
                       [0, 0, 0, 0, 0, 0, 0, 0, 0]]
    sacrifice_sequence = [[6,7], [7,0], [0,1], [1,4], [4,3]]
    last_move = 6
    non_terminal("UCT", "UCT", 5, 100, sacrifice_state, last_move, sacrifice_sequence, "OUTPUT/Non-Terminal-5.txt")
    non_terminal("New", "UCT", 5, 100, sacrifice_state, last_move, sacrifice_sequence, "OUTPUT/Non-Terminal-5.txt")
    non_terminal("Decisive", "UCT", 5, 100, sacrifice_state, last_move, sacrifice_sequence, "OUTPUT/Non-Terminal-5.txt")
    non_terminal("GRAVE", "UCT", 5, 100, sacrifice_state, last_move, sacrifice_sequence, "OUTPUT/Non-Terminal-5.txt")

    non_terminal("UCT", "UCT", 5, 1000, sacrifice_state, last_move, sacrifice_sequence, "OUTPUT/Non-Terminal-5.txt")
    non_terminal("New", "UCT", 5, 1000, sacrifice_state, last_move, sacrifice_sequence, "OUTPUT/Non-Terminal-5.txt")
    non_terminal("Decisive", "UCT", 5, 1000, sacrifice_state, last_move, sacrifice_sequence, "OUTPUT/Non-Terminal-5.txt")
    non_terminal("GRAVE", "UCT", 5, 1000, sacrifice_state, last_move, sacrifice_sequence, "OUTPUT/Non-Terminal-5.txt")

    non_terminal("UCT", "UCT", 5, 10000, sacrifice_state, last_move, sacrifice_sequence, "OUTPUT/Non-Terminal-5.txt")
    non_terminal("New", "UCT", 5, 10000, sacrifice_state, last_move, sacrifice_sequence, "OUTPUT/Non-Terminal-5.txt")
    non_terminal("Decisive", "UCT", 5, 10000, sacrifice_state, last_move, sacrifice_sequence, "OUTPUT/Non-Terminal-5.txt")
    non_terminal("GRAVE", "UCT", 5, 10000, sacrifice_state, last_move, sacrifice_sequence, "OUTPUT/Non-Terminal-5.txt")

##    #for non-terminal 7
    sacrifice_state = [[-1, -1, 1, 1, 0, 1, -1, 1, -1],
                       [-1, -1, -1, 0, 0, 0, 0, 0, 0],
                       [1, 1, 1, 0, 0, 0, 0, 0, 0],
                       [-1, -1, 1, 1, 0, -1, -1, 1, 1],
                       [1, 0, -1, 0, -1, 0, 0, 0, 0],
                       [0, 0, 0, 0, 0, 0, 0, 0, 0],
                       [-1, 0, 0, -1, 0, 0, 0, 0, 0],
                       [1, 1, -1, 1, 1, -1, -1, -1, 0],
                       [0, 0, 0, 1, 0, 0, 1, 0, 0]]
    sacrifice_sequence = [[6,7], [7,8], [8,0], [0,4], [4,3], [3,4], [4,6]]
    last_move = 6
    non_terminal("UCT", "UCT", 7, 100, sacrifice_state, last_move, sacrifice_sequence, "OUTPUT/Non-Terminal-7.txt")
    non_terminal("New", "UCT", 7, 100, sacrifice_state, last_move, sacrifice_sequence, "OUTPUT/Non-Terminal-7.txt")
    non_terminal("Decisive", "UCT", 7, 100, sacrifice_state, last_move, sacrifice_sequence, "OUTPUT/Non-Terminal-7.txt")
    non_terminal("GRAVE", "UCT", 7, 100, sacrifice_state, last_move, sacrifice_sequence, "OUTPUT/Non-Terminal-7.txt")

    non_terminal("UCT", "UCT", 7, 1000, sacrifice_state, last_move, sacrifice_sequence, "OUTPUT/Non-Terminal-7.txt")
    non_terminal("New", "UCT", 7, 1000, sacrifice_state, last_move, sacrifice_sequence, "OUTPUT/Non-Terminal-7.txt")
    non_terminal("Decisive", "UCT", 7, 1000, sacrifice_state, last_move, sacrifice_sequence, "OUTPUT/Non-Terminal-7.txt")
    non_terminal("GRAVE", "UCT", 7, 1000, sacrifice_state, last_move, sacrifice_sequence, "OUTPUT/Non-Terminal-7.txt")

    non_terminal("UCT", "UCT", 7, 10000, sacrifice_state, last_move, sacrifice_sequence, "OUTPUT/Non-Terminal-7.txt")
    non_terminal("New", "UCT", 7, 10000, sacrifice_state, last_move, sacrifice_sequence, "OUTPUT/Non-Terminal-7.txt")
    non_terminal("Decisive", "UCT", 7, 10000, sacrifice_state, last_move, sacrifice_sequence, "OUTPUT/Non-Terminal-7.txt")
    non_terminal("GRAVE", "UCT", 7, 10000, sacrifice_state, last_move, sacrifice_sequence, "OUTPUT/Non-Terminal-7.txt")




    
