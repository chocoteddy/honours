"""
Created By: Lilly (lycho13@student.monash.edu)

-Scripts to run tuning tests.

"""

import time

import sys
sys.path.append('../UCT')
sys.path.append('../DECISIVE')
sys.path.append('../GRAVE')
sys.path.append('../NEW')
sys.path.append('../UTTT')

from Uct import Uct
from Decisive import Decisive
from Grave import Grave
from New import New

from State import State, move_from_states, pretty_print

from Play import play

import grave_settings
import new_settings


def tune_GRAVE_bias(output_file):
    #generate values to test
    values_to_test = []
    for i in range(1,16):
        values_to_test.append(10**(-i))
    #print(values_to_test)

    for value_to_test in values_to_test:
        grave_settings.BIAS = value_to_test
        grave_settings.REF = 0
        grave_win_count = 0
        uct_win_count = 0
        with open(output_file, "a") as results:
            for i in range(100):
                agent_x = Grave(1)
                agent_o = Uct(-1)
                win = play(agent_x, agent_o, 100)
                if win == 1:
                    grave_win_count += 1
                elif win == -1:
                    uct_win_count += 1
            results.write("BIAS = {0}\nGRAVE win count/total non-draw games = {1}/{2}\n".format(value_to_test,
                                                                                                grave_win_count,
                                                                                                grave_win_count + uct_win_count))
            print("{0} {1}/{2}".format(value_to_test, grave_win_count, grave_win_count+uct_win_count))
            
def tune_GRAVE_ref(output_file):
    #generate values to test
    values_to_test = [1, 5, 10, 20, 25, 50, 100, 200, 400]

    for value_to_test in values_to_test:
        grave_settings.REF = value_to_test
        grave_win_count = 0
        uct_win_count = 0
        with open(output_file, "a") as results:
            for i in range(100):
                agent_x = Grave(1)
                agent_o = Uct(-1)
                win = play(agent_x, agent_o, 100)
                if win == 1:
                    grave_win_count += 1
                elif win == -1:
                    uct_win_count += 1
            results.write("REF = {0}\nGRAVE win count/total non-draw games = {1}/{2}\n".format(value_to_test,
                                                                                               grave_win_count,
                                                                                               grave_win_count + uct_win_count))
            print("{0} {1}/{2}".format(value_to_test, grave_win_count, grave_win_count+uct_win_count))
            

def tune_NEW_alpha(output_file):
    #generate values to test
    values_to_test = [x / 100.0 for x in range(5, 100, 5)]
    values_to_test.reverse()
    #print(values_to_test)

    for value_to_test in values_to_test:
        new_settings.ALPHA = value_to_test
        new_win_count = 0
        uct_win_count = 0
        with open(output_file, "a") as results:
            for i in range(100):
                agent_x = New(1)
                agent_o = Uct(-1)
                win = play(agent_x, agent_o, 100)
                if win == 1:
                    new_win_count += 1
                elif win == -1:
                    uct_win_count += 1
            results.write("ALPHA = {0}\nNEW win count/total non-draw games = {1}/{2}\n".format(value_to_test,
                                                                                               new_win_count,
                                                                                               new_win_count + uct_win_count))
            print("{0} {1}/{2}".format(value_to_test, new_win_count, new_win_count+uct_win_count))

            
if __name__ == "__main__":
    tune_GRAVE_bias("OUTPUT/GRAVE_bias.txt")
    tune_GRAVE_ref("OUTPUT/GRAVE_ref.txt")
    tune_NEW_alpha("OUTPUT/NEW_alpha.txt")
            

