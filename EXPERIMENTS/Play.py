"""
Created By: Lilly (lycho13@student.monash.edu)

-Play!

"""

import time

import sys
sys.path.append('../UTTT')

from State import State, move_from_states, pretty_print, get_possible_next_moves

import copy

def play(agent_x, agent_o, iterations_per_turn):
    state = State([[0]*9 for i in range(9)], None, 1)

    while not state.draw() and state.win() == 0: #while not draw and not win
        #print(self.state)
        
        #iteration or time constraint here
        for i in range(iterations_per_turn): 
            agent_x.iterate()
            agent_o.iterate()

        #set active player/sleep player
        if state.next_player == 1:
            agent_active = agent_x
            agent_sleep = agent_o
        else:
            agent_active = agent_o
            agent_sleep = agent_x

        #get best move so far
        best_child = agent_active.root.best_child()
        best_move = move_from_states(state, best_child.state)

        #set new root for agents
        agent_active.root = best_child
        for child in agent_sleep.root.children:
            if move_from_states(state, child.state) == best_move:
                agent_sleep.root = child
                break

        #play move
        state.move(best_move[0], best_move[1])

    return state.win() #1 for X, -1 for O, 0 for draw

def play_sacrifice(sacrifice_state, last_move, agent_x, agent_o, turns, iterations_per_turn):
    moves_history = []
    state = State(copy.deepcopy(sacrifice_state), last_move, 1)
    agent_x.root.state = copy.deepcopy(state)
    agent_o.root.state = copy.deepcopy(state)

    for i in range(turns):
        #print(self.state)
        
        #iteration or time constraint here
        for i in range(iterations_per_turn): 
            agent_x.iterate()
            agent_o.iterate()

        #set active player/sleep player
        if state.next_player == 1:
            agent_active = agent_x
            agent_sleep = agent_o
        else:
            agent_active = agent_o
            agent_sleep = agent_x

        #get best move so far
        best_child = agent_active.root.best_child()
        #print(move_from_states(state, best_child.state))
        best_move = move_from_states(state, best_child.state)

        #set new root for agents
        agent_active.root = best_child
        for child in agent_sleep.root.children:
            if move_from_states(state, child.state) == best_move:
                agent_sleep.root = child
                break

        #play move
        state.move(best_move[0], best_move[1])
        moves_history.append(best_move)

    return moves_history


if __name__ == "__main__":
    pass
            

