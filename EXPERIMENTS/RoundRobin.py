"""
Created By: Lilly (lycho13@student.monash.edu)

-Scripts to run round robin tests.

"""

import time

import sys
sys.path.append('../UCT')
sys.path.append('../DECISIVE')
sys.path.append('../GRAVE')
sys.path.append('../NEW')
sys.path.append('../UTTT')

from Uct import Uct
from Decisive import Decisive
from Grave import Grave
from New import New

from State import State, move_from_states, pretty_print

from Play import play

def mcts_variant(variant_name, player_token):
    variant_name = variant_name.capitalize()
    return globals()[variant_name](player_token)

def round_A_B(A, B, output_file):
    A_win_count = 0
    B_win_count = 0
    with open(output_file, "a") as results:
        for i in range(100):
            agent_x = mcts_variant(A, 1)
            agent_o = mcts_variant(B, -1)
            win = play(agent_x, agent_o, 100)
            if win == 1:
                A_win_count += 1
            elif win == -1:
                B_win_count += 1
        results.write("with {0} vs. {1}\n{0} win count/total non-draw games = {2}/{3}\n".format(A, B,
                                                                                                A_win_count,
                                                                                                A_win_count + B_win_count))
        print("{0}/{1}".format(A_win_count, A_win_count+B_win_count))

        
if __name__ == "__main__":
    round_A_B("UCT", "New", "OUTPUT/UCT-New.txt")
    round_A_B("UCT", "Decisive", "OUTPUT/UCT-Decisive.txt")
    round_A_B("UCT", "GRAVE", "OUTPUT/UCT-GRAVE.txt")
    round_A_B("New", "Decisive", "OUTPUT/New-Decisive.txt")
    round_A_B("New", "GRAVE", "OUTPUT/New-GRAVE.txt")
    round_A_B("Decisive", "GRAVE", "OUTPUT/Decisive-GRAVE.txt")

    round_A_B("New", "UCT", "OUTPUT/New-UCT.txt")
    round_A_B("Decisive", "UCT", "OUTPUT/Decisive-UCT.txt")
    round_A_B("GRAVE", "UCT", "OUTPUT/GRAVE-UCT.txt")
    round_A_B("Decisive", "New", "OUTPUT/Decisive-New.txt")
    round_A_B("GRAVE", "New", "OUTPUT/GRAVE-New.txt")
    round_A_B("GRAVE", "Decisive", "OUTPUT/GRAVE-Decisive.txt")
            

