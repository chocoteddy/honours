"""
Created By: Lilly (lycho13@student.monash.edu)

-This is the game.

"""

import time

import sys
sys.path.append('../MCTS')
sys.path.append('../UCT')
sys.path.append('../DECISIVE')
sys.path.append('../GRAVE')
sys.path.append('../NEW')

from Uct import Uct
from Decisive import Decisive
from Grave import Grave
from New import New

from State import State, move_from_states, pretty_print

class UTTT:
    def __init__(self):
        #initialize empty board, with no last move, and player 1 begins
        self.state = State([[0]*9 for i in range(9)], None, 1)
        self.agent_x = Grave(1)
        self.agent_o = Decisive(-1)

    def play(self):
        start = time.time()
        while not self.state.draw() and self.state.win() == 0: #while not draw and not win
            #print(self.state)
            
            #iteration or time constraint here
            for i in range(100): 
                self.agent_x.iterate()
                self.agent_o.iterate()

            #set active player/sleep player
            if self.state.next_player == 1:
                agent_active = self.agent_x
                agent_sleep = self.agent_o
            else:
                agent_active = self.agent_o
                agent_sleep = self.agent_x

            #get best move so far
            best_child = agent_active.root.best_child()
            best_move = move_from_states(self.state, best_child.state)

            #set new root for agents
            agent_active.root = best_child
            for child in agent_sleep.root.children:
                if move_from_states(self.state, child.state) == best_move:
                    agent_sleep.root = child
                    break

            #play move
            self.state.move(best_move[0], best_move[1])

        end = time.time()
        print("winner:", self.state.win())
        pretty_print(self.state)
        print("time taken:", end - start)


if __name__ == "__main__":
    ut3 = UTTT()
    ut3.play()
            

