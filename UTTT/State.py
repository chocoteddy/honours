"""
Created By: Lilly (lycho13@student.monash.edu)

-Used to represent a state of the board.
e.g.  0 │ 1 │ 2
     ───┼───┼───
      3 │ 4 │ 5
     ───┼───┼───
      6 │ 7 │ 8  is indexed by the list [0, 1, 2, 3, 4, 5, 6, 7, 8].
      

      
-Players X and O are represented by 1 and -1 respectively.
e.g.    │   │ X
     ───┼───┼───
        │ O │  
     ───┼───┼───
        │   │    is represented by the list [0, 0, 1, 0, -1, 0, 0, 0, 0]
        
"""

import unittest
import copy

def pretty_print(state):
    board = copy.deepcopy(state.board)
    for inner in board:
        for i in range(9):
            if inner[i] == -1:
                inner[i] = "O"
            elif inner[i] == 1:
                inner[i] = "X"
            else:
                inner[i] = " "

    print("              │               │              ")

    #first row of outer board
    print("  {0} │ {1} │ {2}   │   {3} │ {4} │ {5}   │   {6} │ {7} │ {8}".format(board[0][0], board[0][1], board[0][2],
                                                                                 board[1][0], board[1][1], board[1][2],
                                                                                 board[2][0], board[2][1], board[2][2]))
    print(" ───┼───┼───  │  ───┼───┼───  │  ───┼───┼───")
    print("  {0} │ {1} │ {2}   │   {3} │ {4} │ {5}   │   {6} │ {7} │ {8}".format(board[0][3], board[0][4], board[0][5],
                                                                                 board[1][3], board[1][4], board[1][5],
                                                                                 board[2][3], board[2][4], board[2][5]))
    print(" ───┼───┼───  │  ───┼───┼───  │  ───┼───┼───")
    print("  {0} │ {1} │ {2}   │   {3} │ {4} │ {5}   │   {6} │ {7} │ {8}".format(board[0][6], board[0][7], board[0][8],
                                                                                 board[1][6], board[1][7], board[1][8],
                                                                                 board[2][6], board[2][7], board[2][8]))
    print("──────────────┼───────────────┼──────────────")

    #second row of outer board
    print("  {0} │ {1} │ {2}   │   {3} │ {4} │ {5}   │   {6} │ {7} │ {8}".format(board[3][0], board[3][1], board[3][2],
                                                                                 board[4][0], board[4][1], board[4][2],
                                                                                 board[5][0], board[5][1], board[5][2]))
    print(" ───┼───┼───  │  ───┼───┼───  │  ───┼───┼───")
    print("  {0} │ {1} │ {2}   │   {3} │ {4} │ {5}   │   {6} │ {7} │ {8}".format(board[3][3], board[3][4], board[3][5],
                                                                                 board[4][3], board[4][4], board[4][5],
                                                                                 board[5][3], board[5][4], board[5][5]))
    print(" ───┼───┼───  │  ───┼───┼───  │  ───┼───┼───")
    print("  {0} │ {1} │ {2}   │   {3} │ {4} │ {5}   │   {6} │ {7} │ {8}".format(board[3][6], board[3][7], board[3][8],
                                                                                 board[4][6], board[4][7], board[4][8],
                                                                                 board[5][6], board[5][7], board[5][8]))
    print("──────────────┼───────────────┼──────────────")

    #third row of outer board
    print("  {0} │ {1} │ {2}   │   {3} │ {4} │ {5}   │   {6} │ {7} │ {8}".format(board[6][0], board[6][1], board[6][2],
                                                                                 board[7][0], board[7][1], board[7][2],
                                                                                 board[8][0], board[8][1], board[8][2]))
    print(" ───┼───┼───  │  ───┼───┼───  │  ───┼───┼───")
    print("  {0} │ {1} │ {2}   │   {3} │ {4} │ {5}   │   {6} │ {7} │ {8}".format(board[6][3], board[6][4], board[6][5],
                                                                                 board[7][3], board[7][4], board[7][5],
                                                                                 board[8][3], board[8][4], board[8][5]))
    print(" ───┼───┼───  │  ───┼───┼───  │  ───┼───┼───")
    print("  {0} │ {1} │ {2}   │   {3} │ {4} │ {5}   │   {6} │ {7} │ {8}".format(board[6][6], board[6][7], board[6][8],
                                                                                 board[7][6], board[7][7], board[7][8],
                                                                                 board[8][6], board[8][7], board[8][8]))
    print("              │               │              ")

def move_from_states(before_state, after_state):
    #state ordering doesn't really matter
    #will return first difference if more than one present
    before_state = before_state.board
    after_state = after_state.board
    for i in range(9):
        if before_state[i] != after_state[i]:
            for j in range(9):
                if before_state[i][j] != after_state[i][j]:
                    return [i, j]
    return None

def get_possible_next_moves(state):    
    possible_next_moves = []
    if state.last_move == None: #if first move
        for i in range(9):
            for j in range(9):
                possible_next_moves.append([i, j])
    else:
        inner_grid = state.board[state.last_move] #playable grid

        if state.inner_win(inner_grid) == 0 and not state.inner_draw(inner_grid): #if inner grid is not win and not draw
            for i in range(9):
                if inner_grid[i] == 0:
                    possible_next_moves.append([state.last_move, i]) #[outer, inner]

        else: #inner grid is won or draw
            for i in range(9):
                if state.inner_win(state.board[i]) == 0 and not state.inner_draw(state.board[i]):
                    for j in range(9):
                        if state.board[i][j] == 0:
                            possible_next_moves.append([i, j])

    return possible_next_moves

class State():
    def __init__(self, board, last_move, next_player):
        self.board = board
        self.last_move = last_move #inner
        self.next_player = next_player

    def __str__(self):
        string = "*"
        for i in range(0, 7, 3):
            string += str(self.board[i]) + str(self.board[i+1]) + str(self.board[i+2])
            if i != 6:
                string += "\n"
            
        return string

    def inner_win(self, inner_board):
        #!!!!!IMPORTANT - This method relies on the player's markers being 1 and -1 to function correctly.
        
        #create winning possibilities
        row1 = inner_board[:3]
        row2 = inner_board[3:6]
        row3 = inner_board[6:]
        col1 = inner_board[::3]
        col2 = inner_board[1::3]
        col3 = inner_board[2::3]
        diagfront = inner_board[2:7:2]
        diagback = inner_board[::4]

        possibilities = [row1, row2, row3,
                         col1, col2, col3,
                         diagfront, diagback]

        #if any possibility sums to 3 or -3 then it is a win (3 for player X, -3 for player O)
        sums = [sum(p) for p in possibilities]
        for s in sums:
            if s == 3:
                return 1
            elif s == -3:
                return -1
        return 0

    """
    Returns:  1   if player X won;
              -1  if player O won;
              0   otherwise.
    """
    def win(self):
        #checks if either player has won, uses self.inner_win
        outer_board = []
        for inner_board in self.board:
            outer_board.append(self.inner_win(inner_board))

        #let's re-use self.inner_win
        result = self.inner_win(outer_board)
        return result

    def inner_draw(self, inner_board):
        if 0 in inner_board:
            return False
        return True

    def draw(self):
        if get_possible_next_moves(self) == []:
            return True
        return False

    def move(self, outer, inner):
        new_board = self.board
        new_board[outer][inner] = self.next_player
        self.board = new_board

        self.last_move = inner
        self.next_player = -self.next_player

class TestMoveFromStates(unittest.TestCase):
    def test_move_from_states(self):
        s1 = State([[0, 0, 0, 0, 0, 0, 0, 0, 0],
                    [0, 0, 0, 0, 0, 0, 0, 0, 0],
                    [0, 0, 0, 0, 0, 0, 0, 0, 0],
                    [0, 0, 0, 0, 0, 0, 0, 0, 0],
                    [0, 0, 0, 0, 0, 0, 0, 0, 0],
                    [0, 0, 0, 0, 0, 0, 0, 0, 0],
                    [0, 0, 0, 0, 0, 0, 0, 0, 0],
                    [0, 0, 0, 0, 0, 0, 0, 0, 0],
                    [0, 0, 0, 0, 0, 0, 0, 0, 0]], None, 1)
        self.assertIsNone(move_from_states(s1, s1))
        
        s2 = State([[0, 0, 0, 0, 0, 0, 0, 0, 0],
                    [0, 0, 0, 0, 0, 0, 0, 0, 0],
                    [0, 1, 0, 0, 0, 0, 0, 0, 0],
                    [0, 0, 0, 0, 0, 0, 0, 0, 0],
                    [0, 0, 0, 0, 0, 0, 0, 0, 0],
                    [0, 0, 0, 0, 0, 0, 0, 0, 0],
                    [0, 0, 0, 0, 0, 0, 0, 0, 0],
                    [0, 0, 0, 0, 0, 0, 0, 0, 0],
                    [0, 0, 0, 0, 0, 0, 0, 0, 0]], None, 1)
        self.assertEqual(move_from_states(s1, s2), [2, 1])


class TestGetPossibleNextMoves(unittest.TestCase):
    def test_get_possible_next_moves(self):
        s = State([[0]*9 for i in range(9)], None, 1)
        self.assertEqual(get_possible_next_moves(s), [[0, 0], [0, 1], [0, 2], [0, 3], [0, 4], [0, 5], [0, 6], [0, 7], [0, 8],
                                                      [1, 0], [1, 1], [1, 2], [1, 3], [1, 4], [1, 5], [1, 6], [1, 7], [1, 8],
                                                      [2, 0], [2, 1], [2, 2], [2, 3], [2, 4], [2, 5], [2, 6], [2, 7], [2, 8],
                                                      [3, 0], [3, 1], [3, 2], [3, 3], [3, 4], [3, 5], [3, 6], [3, 7], [3, 8],
                                                      [4, 0], [4, 1], [4, 2], [4, 3], [4, 4], [4, 5], [4, 6], [4, 7], [4, 8],
                                                      [5, 0], [5, 1], [5, 2], [5, 3], [5, 4], [5, 5], [5, 6], [5, 7], [5, 8],
                                                      [6, 0], [6, 1], [6, 2], [6, 3], [6, 4], [6, 5], [6, 6], [6, 7], [6, 8],
                                                      [7, 0], [7, 1], [7, 2], [7, 3], [7, 4], [7, 5], [7, 6], [7, 7], [7, 8],
                                                      [8, 0], [8, 1], [8, 2], [8, 3], [8, 4], [8, 5], [8, 6], [8, 7], [8, 8]])
        s.last_move = 1
        self.assertEqual(get_possible_next_moves(s), [[1, 0], [1, 1], [1, 2], [1, 3], [1, 4], [1, 5], [1, 6], [1, 7], [1, 8]])

        s.board[1] = [1, 0, 0, 0, 1, 0, 1, 0, 0]
        self.assertEqual(get_possible_next_moves(s), [[1, 1], [1, 2], [1, 3], [1, 5], [1, 7], [1, 8]])

        #case draw/win, play anywhere
        s.board = [[1, 0, 0, 1, 0, 0, 1, 0, 0],
                   [1, 1, 1, 1, 1, 1, 1, 1, 1],
                   [1, 1, 0, 1, 0, 1, 0, 1, 0],
                   [0, 0, 0, 0, 0, 0, 0, 0, 0],
                   [1, 1, 1, 1, 1, 1, 1, 1, 1],
                   [1, 1, 1, 1, 1, 1, 1, 1, 1],
                   [1, 1, 1, 1, 1, 1, 1, 1, 1],
                   [1, 1, 1, 1, 1, 1, 1, 1, 1],
                   [1, 1, 1, 1, 1, 1, 1, 1, 1]]
        self.assertEqual(get_possible_next_moves(s), [[2, 2], [2, 4], [2, 6], [2, 8],
                                                      [3, 0], [3, 1], [3, 2], [3, 3], [3, 4], [3, 5], [3, 6], [3, 7], [3, 8]])

        s.board = [[1]*9 for i in range(9)]
        self.assertEqual(get_possible_next_moves(s), [])

        
class TestState(unittest.TestCase):
    def test_init(self):
        s = State([[0]*9 for i in range(9)], None, 1)
        self.assertEqual(s.board, [[0, 0, 0, 0, 0, 0, 0, 0, 0],
                                   [0, 0, 0, 0, 0, 0, 0, 0, 0],
                                   [0, 0, 0, 0, 0, 0, 0, 0, 0],
                                   [0, 0, 0, 0, 0, 0, 0, 0, 0],
                                   [0, 0, 0, 0, 0, 0, 0, 0, 0],
                                   [0, 0, 0, 0, 0, 0, 0, 0, 0],
                                   [0, 0, 0, 0, 0, 0, 0, 0, 0],
                                   [0, 0, 0, 0, 0, 0, 0, 0, 0],
                                   [0, 0, 0, 0, 0, 0, 0, 0, 0]])
        self.assertIsNone(s.last_move)
        self.assertEqual(s.next_player, 1)

    """
    def test_str(self):
        s = State([[0]*9 for i in range(9)], None, 1)
        self.assertEqual(str(s), "[0, 0, 0, 0, 0, 0, 0, 0, 0][0, 0, 0, 0, 0, 0, 0, 0, 0][0, 0, 0, 0, 0, 0, 0, 0, 0]\n[0, 0, 0, 0, 0, 0, 0, 0, 0][0, 0, 0, 0, 0, 0, 0, 0, 0][0, 0, 0, 0, 0, 0, 0, 0, 0]\n[0, 0, 0, 0, 0, 0, 0, 0, 0][0, 0, 0, 0, 0, 0, 0, 0, 0][0, 0, 0, 0, 0, 0, 0, 0, 0]")
    """
    
    def test_inner_win(self):
        s = State([[0]*9 for i in range(9)], None, 1)
        self.assertEqual(s.inner_win([0]*9), 0)

        #player 1 wins
        self.assertEqual(s.inner_win([1]*3 + [0]*6), 1)
        self.assertEqual(s.inner_win([0]*3 + [1]*3 + [0]*3), 1)
        self.assertEqual(s.inner_win([0]*6 + [1]*3), 1)
        self.assertEqual(s.inner_win([1, 0, 0]*3), 1)
        self.assertEqual(s.inner_win([0, 1, 0]*3), 1)
        self.assertEqual(s.inner_win([0, 0, 1]*3), 1)
        self.assertEqual(s.inner_win([0, 0, 1, 0, 1, 0, 1, 0, 0]), 1)
        self.assertEqual(s.inner_win([1, 0, 0, 0, 1, 0, 0, 0, 1]), 1)

        #player 2 wins
        self.assertEqual(s.inner_win([-1]*3 + [0]*6), -1)
        self.assertEqual(s.inner_win([0]*3 + [-1]*3 + [0]*3), -1)
        self.assertEqual(s.inner_win([0]*6 + [-1]*3), -1)
        self.assertEqual(s.inner_win([-1, 0, 0]*3), -1)
        self.assertEqual(s.inner_win([0, -1, 0]*3), -1)
        self.assertEqual(s.inner_win([0, 0, -1]*3), -1)
        self.assertEqual(s.inner_win([0, 0, -1, 0, -1, 0, -1, 0, 0]), -1)
        self.assertEqual(s.inner_win([-1, 0, 0, 0, -1, 0, 0, 0, -1]), -1)

        #extra
        self.assertEqual(s.inner_win([1, 0, 0, 0, 0, -1, 0, 0, 0]), 0)
        self.assertEqual(s.inner_win([1, -1, 1, 0, -1, -1, 1, 0, 1]), 0)
        self.assertEqual(s.inner_win([1, -1, 1, 1, -1, -1, -1, 1, 1]), 0)
        self.assertEqual(s.inner_win([1, 0, -1, 0, 1, -1, 0, 0, 1]), 1)
        self.assertEqual(s.inner_win([1, 1, 1, 1, -1, -1, -1, -1, 1]), 1)
        self.assertEqual(s.inner_win([0, 1, -1, 1, -1, -1, 0, 1, -1]), -1)
        self.assertEqual(s.inner_win([-1, 1, 1, 1, 1, -1, -1, -1, -1]), -1)

    def test_win(self):
        s = State([[0]*9 for i in range(9)], None, 1)
        self.assertEqual(s.win(), 0)
        
        s.board = [[1, 0, 0, 0, 0, -1, 0, 0, 0],
                   [1, 0, -1, 0, 1, -1, 0, 0, 1],
                   [0, 1, -1, 1, -1, -1, 0, 1, -1],
                   [1, -1, 1, 0, -1, -1, 1, 0, 1],
                   [1, 1, 1, 1, -1, -1, -1, -1, 1],
                   [-1, 1, 1, 1, 1, -1, -1, -1, -1],
                   [0, 0, 1, 0, 1, 0, 1, 0, 0],
                   [-1, 1, 1, -1, 0, 1, 0, -1, 1],
                   [1, -1, 1, 1, -1, -1, -1, 1, 1]]
        self.assertEqual(s.win(), 1)

        s.board = [[0, 1, -1, 1, -1, -1, 0, 1, -1],
                   [1, 0, 0, 0, 0, -1, 0, 0, 0],
                   [1, 0, -1, 0, 1, -1, 0, 0, 1],
                   [1, -1, 1, 0, -1, -1, 1, 0, 1],
                   [-1, 1, 1, 1, 1, -1, -1, -1, -1],
                   [1, 1, 1, 1, -1, -1, -1, -1, 1],
                   [1, -1, 1, 1, -1, -1, -1, 1, 1],
                   [0, 0, 0, 0, 0, 0, 0, 0, 0],
                   [-1, 0, 0, 0, -1, 0, 0, 0, -1]]
        self.assertEqual(s.win(), -1)

    def test_inner_draw(self):
        s = State([[0]*9 for i in range(9)], None, 1)
        self.assertFalse(s.inner_draw([0]*9))

        self.assertFalse(s.inner_draw([1, -1, 1, 0, -1, -1, 1, 0, 1]))
        self.assertTrue(s.inner_draw([1, 1, 1, 1, -1, -1, -1, -1, 1]))

    def test_draw(self):
        s = State([[0]*9 for i in range(9)], None, 1)
        self.assertFalse(s.draw())

        s = State([[0]*9 for i in range(9)], 0, 1)
        self.assertFalse(s.draw())

        s.board = [[1]*9 for i in range(9)]
        self.assertTrue(s.draw())

        s.board = [[1, 0, 0, 1, 0, 0, 1, 0, 0] for i in range(9)]
        self.assertTrue(s.draw())

        s.board = [[1, 1, 1, 1, 1, 1, 1, 1, 1] for i in range(8)]
        s.board.insert(0, [1, 0, 0, 1, 0, 0, 1, 0, 0])
        self.assertTrue(s.draw())

    def test_move(self):
        s = State([[0]*9 for i in range(9)], None, 1)
        s.move(0, 0)
        self.assertEqual(s.last_move, 0)
        self.assertEqual(s.next_player, -1)
        self.assertEqual(s.board[0], [1, 0, 0, 0, 0, 0, 0, 0, 0])

        s.move(0, 8)
        self.assertEqual(s.last_move, 8)
        self.assertEqual(s.next_player, 1)
        self.assertEqual(s.board[0], [1, 0, 0, 0, 0, 0, 0, 0, -1])

        s.move(8, 1)
        self.assertEqual(s.last_move, 1)
        self.assertEqual(s.next_player, -1)
        self.assertEqual(s.board[8], [0, 1, 0, 0, 0, 0, 0, 0, 0])

if __name__ == "__main__":
    unittest.main()
