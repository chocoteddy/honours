"""
Created By: Lilly (lycho13@student.monash.edu)

"""

import sys
sys.path.append('../UTTT')
from State import State, get_possible_next_moves

import random
import copy

from DecisiveNode import DecisiveNode, decisive_score

class Decisive():
    def __init__(self, player_id):
        #initialize root node with empty board, None last move and next player 1
        self.player_id = player_id #must be 1 or -1
        self.root = DecisiveNode(State([[0]*9 for i in range(9)], None, 1))

    def __str__(self):
        return str(self.root)

    def select(self):
        return self.root.next_child()

    def simulate(self, node):
        current_state = copy.deepcopy(node.state)
        while not current_state.draw() and current_state.win() == 0: #while not draw and not win
            possible_next_moves = get_possible_next_moves(current_state)

            chosen_move = None
            for move in possible_next_moves:
                tmp_state = copy.deepcopy(node.state)
                tmp_state.move(move[0], move[1])
                if tmp_state.inner_win(tmp_state.board[move[0]]) == self.player_id: #if inner win
                    chosen_move = move
                    if tmp_state.win() == self.player_id: #if outer win
                        break
                del tmp_state
            
            if chosen_move == None: #if no winning move, choose random
                chosen_move = possible_next_moves[random.randint(0, len(possible_next_moves)-1)]

            current_state.move(chosen_move[0], chosen_move[1])

        #___simulation has ended breakpoint.

        win = current_state.win()
        del current_state
        
        return win

    def expand(self, node, win):
        if win == self.player_id:
            node.win_count += 1
        node.total_simulations += 1

        if node.parent is not None: #if not root
            node.score = decisive_score(node)

    def backpropagate(self, node, win):
        #MCTS backpropagation
        current = node.parent
        while current is not None: #while not root
            if win == self.player_id: 
                current.win_count += 1
            current.total_simulations += 1
            current.score = decisive_score(current)
            
            current = current.parent

    def iterate(self):
        selected_node = self.select()
        result = self.simulate(selected_node) 
        #print(result)
        self.expand(selected_node, result)
        self.backpropagate(selected_node, result)
        
        
if __name__ == "__main__":
    u = Decisive(-1)  #!!!!!FOR PLAYER X, USE -1 FOR PLAYER O
    for i in range(1000):
        u.iterate()
    print(u)
    
