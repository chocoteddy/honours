"""
Created By: Lilly (lycho13@student.monash.edu)

"""

import sys
sys.path.append('../UTTT')
from State import State, get_possible_next_moves

import random
import copy

from Node import Node, score

class Mcts():
    def __init__(self, player_id):
        #initialize root node with empty board, None last move and next player 1
        self.player_id = player_id
        self.root = Node(State([[0]*9 for i in range(9)], None, 1))

    def __str__(self):
        return str(self.root)

    def select(self):
        return self.root.next_child()

    def simulate(self, node):
        current_state = copy.deepcopy(node.state)
        while not current_state.draw() and current_state.win() == 0: #while not draw and not win
            possible_next_moves = get_possible_next_moves(current_state)
            
            chosen_move = possible_next_moves[random.randint(0, len(possible_next_moves)-1)]

            current_state.move(chosen_move[0], chosen_move[1])

        #___simulation has ended breakpoint.

        win = current_state.win()
        del current_state
        
        return win

    def expand(self, node, win):
        if win == self.player_id:
            node.win_count += 1
        node.total_simulations += 1

    def backpropagate(self, node, win):
        parent = node.parent
        while parent is not None: #while not root
            if win == self.player_id: 
                parent.win_count += 1
            parent.total_simulations += 1
            parent = parent.parent

    def iterate(self):
        selected_node = self.select()
        result = self.simulate(selected_node) 
        #print(result)
        self.expand(selected_node, result)
        self.backpropagate(selected_node, result)
        
        
if __name__ == "__main__":
    m = Mcts(1) #!!!!!FOR PLAYER X, USE -1 FOR PLAYER O
    for i in range(100):
        m.iterate()
    print(m)
    
