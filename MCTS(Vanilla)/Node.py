"""
Created By: Lilly (lycho13@student.monash.edu)

"""

import sys
sys.path.append('../UTTT')
from State import State, get_possible_next_moves

import copy
import unittest
import random

def score(node):
    if node.total_simulations == 0:
        return float("inf")
    return node.win_count/node.total_simulations

class Node():
    def __init__(self, state):
        self.state = state
        self.parent = None
        self.children = [] #list of children nodes

        self.win_count = 0
        self.total_simulations = 0

    def __str__(self, level=0):
        string = "\t"*level + repr(self).replace("\n", "\n" + "\t"*level) + "\n"
        for child in self.children:
            string += child.__str__(level+1)
        return string

    def __repr__(self):
        string = str(self.win_count) + "/" + str(self.total_simulations)
        return str(string)

    def add_child(self, child):
        #creates the association between parent and child nodes
        self.children.append(child)
        child.parent = self

    def best_child(self):
        child_score = [score(child) for child in self.children]
        #print(child_score)
        
        max_score = max(child_score)
        max_child_index = [i for i, s in enumerate(child_score) if s == max_score] #get all maximums
        best_child_index = max_child_index[random.randint(0, len(max_child_index)-1)]
        
        best_child = self.children[best_child_index] #choose random maximum
        return best_child

    def next_child(self):        
        if self.children == []: #if leaf node
            #create all child nodes
            possible_next_moves = get_possible_next_moves(self.state)
            #print(possible_next_moves)
            for move in possible_next_moves:
                child = Node(copy.deepcopy(self.state))
                child.state.move(move[0], move[1])
                self.add_child(child)
            if self.total_simulations == 0:
                return self
            #else: return random child
            print(self.children)
            return self.children[random.randint(0, len(self.children)-1)]
            
        else:
            
            #recursive action
            best_child = self.best_child()

            if score(best_child) == float("inf"): #if child left to be added
                return best_child
            else:
                return best_child.next_child()

        
class TestNode(unittest.TestCase):
    def test_init(self):
        s = State([0]*9, 0, 1)
        n = Node(s)
        self.assertEqual(n.state, s)
        self.assertIsNone(n.parent)
        self.assertEqual(n.children, [])
        self.assertEqual(n.win_count, 0)
        self.assertEqual(n.total_simulations, 0)

    def test_add_child(self):
        s = State([0]*9, 0, 1)
        n = Node(s)
        self.assertEqual(n.children, [])

        c = Node(s)
        n.add_child(c)
        self.assertEqual(n.children, [c])
        self.assertEqual(c.parent, n)
        d = Node(s)
        n.add_child(d)
        self.assertEqual(n.children, [c, d])
        self.assertEqual(c.parent, n)
        self.assertEqual(d.parent, n)

    def test_best_child(self):
        s = State([0]*9, 0, 1)
        n = Node(s)
        c1 = Node(s)
        c2 = Node(s)
        c3 = Node(s)

        n.add_child(c1)
        n.add_child(c2)
        n.add_child(c3)

        n.win_count = 1
        n.total_simulations = 1
        c1.win_count = 1
        c1.total_simulations = 1
        c2.total_simulations = 1
        c3.total_simulations = 1

        self.assertEqual(n.best_child(), c1)

    def test_next_child(self):
        s = State([[0]*9 for i in range(9)], None, 1) #root state
        n = Node(s) #root node
        self.assertEqual(n.next_child(), n)

        n.total_simulations = 1
        random_node = n.next_child()
        self.assertIn(random_node, n.children)

        #set random node score to 1/1
        random_node_index = n.children.index(random_node)
        random_node.win_count = 1
        random_node.total_simulations = 1
        #finish first level
        for i in range(1, 81):
            current = n.next_child()
            current.total_simulations = 1
            
        random_node_2 = n.next_child()
        self.assertIn(random_node_2, n.children[random_node_index].children)
        

if __name__ == "__main__":
    unittest.main()
