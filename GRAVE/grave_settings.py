"""
Created By: Lilly (lycho13@student.monash.edu)

"""

global C
global BIAS
global REF

C = 0.4
BIAS = 1e-12
REF = 5
