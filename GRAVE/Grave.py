"""
Created By: Lilly (lycho13@student.monash.edu)

"""

import sys
sys.path.append('../UTTT')
from State import State, get_possible_next_moves, move_from_states

import random
import copy

from GraveNode import GraveNode, grave_score

class Grave():
    def __init__(self, player_id):
        #initialize root node with empty board, None last move and next player 1
        self.player_id = player_id
        self.root = GraveNode(State([[0]*9 for i in range(9)], None, 1))

    def __str__(self):
        return str(self.root)

    def select(self):
        return self.root.next_child()

    def simulate(self, node):
        current_state = copy.deepcopy(node.state)
        moves_played = [] #used to update RAVE siblings
        while not current_state.draw() and current_state.win() == 0: #while not draw and not win
            possible_next_moves = get_possible_next_moves(current_state)
            
            chosen_move = possible_next_moves[random.randint(0, len(possible_next_moves)-1)]
            moves_played.append(chosen_move)

            current_state.move(chosen_move[0], chosen_move[1])

        #___simulation has ended breakpoint.

        win = current_state.win()
        del current_state
        
        return win, moves_played

    def expand(self, node, win):
        """Nodes are really created at the selection stage with win_count, total_simulations = 0.
        A node is not considered 'fully created' until it has total_simulations >= 1"""
        if win == self.player_id: 
            node.win_count += 1
        node.total_simulations += 1

        if node.parent is not None: #if not root
            node.score = grave_score(node)

    def backpropagate(self, node, win, moves_played):
        #MCTS backpropagation
        current = node.parent
        while current is not None: #while not root
            if win == self.player_id: 
                current.win_count += 1
            current.total_simulations += 1
            current.score = grave_score(current)
            
            current = current.parent

        #RAVE backpropagation
        if node.parent is not None: #if not root
            odd_moves_played = moves_played[::2]
            even_moves_played = moves_played[1::2]
            even_moves_played.insert(0, move_from_states(node.parent.state, node.state))

            current = node
            odd = False #parity switch
            while current.parent is not None:
                siblings = current.get_siblings()
                if odd:
                    for s in siblings:
                        s_move = move_from_states(s.parent.state, s.state)
                        if s_move in odd_moves_played:
                            if win == self.player_id:
                                s.AMAF_win_count += 1
                            s.AMAF_total_simulations += 1
                            if s.total_simulations > 0:
                                s.score = grave_score(s)

                else: #even
                    for s in siblings:
                        s_move = move_from_states(s.parent.state, s.state)
                        if s_move in even_moves_played:
                            if win == self.player_id:
                                s.AMAF_win_count += 1
                            s.AMAF_total_simulations += 1
                            if s.total_simulations > 0:
                                s.score = grave_score(s)

                current = current.parent
                odd = not odd      

    def iterate(self):
        selected_node = self.select()
        result, moves_played_in_simulation = self.simulate(selected_node) 
        #print(result)
        self.expand(selected_node, result)
        self.backpropagate(selected_node, result, moves_played_in_simulation)
        
        
if __name__ == "__main__":
    u = Grave(1) #!!!!!FOR PLAYER X, USE -1 FOR PLAYER O
    for i in range(1000):
        u.iterate()
    print(u)
    
