# Welcome to Lilly's Honour's Project!✨ #
゜・。・゜・While you're here, please feel free to have a cookie -> 🍪

> My final honours thesis can be found [here](https://drive.google.com/file/d/1hkUNDzHBEnCJ1g4RwtZSliRdsbz9DZCD/view)!

## 🌼 An overview ##
 * __MCTS(Vanilla)__ is my original, first-mint implementation of basic Monte Carlo Tree Search. Nothing interesting here. Exists solely as a fallback if I ever get lost throughly enough in my own code.
 * __UCT__ is an implementation of Upper Confidence Bound for Tree i.e. UCT-MCTS. It is the most commonly "benchmarked against" variant of MCTS.
 * __DECISIVE__ is an implementation of Decisive-MCTS. It is the algorithm which has shown the most promising potential in sacrifice scenarios.
 * __GRAVE__ is an implementation of Generalized Rapid Action Value Estimation-MCTS. It is one of the more relevant/recent MCTS variant available (published 2015).
 * __NEW__ is an implementation of our proposed improvement, SA-MCTS. It selects a node using exponentially diminishing probabilities which correlate to node values.
 * __UTTT__ defines Ultimate Tic-tac-toe game states and rules.
 * __EXPERIMENTS__ contain script setups for running proposed tests and experiments. Results are stored as plain text files (.txt) in a subfolder called __OUTPUT__.

## 🦄 Definitions ##
* Trees in MCTS are not DAGs.
* In each iteration of MCTS, _exactly one_ simulation is played and _exactly one_ node is created.

![MCTS iterations](https://docs.google.com/uc?id=0B-Vq2G3EGHHGeXhteFA0a2VmTm8)

* At each __move/turn__ of a given __game/trial__ in UTTT, both players have the same time/__iteration__ rounds to "grow" their tree, but one must play a move by the end of the turn.

> P/s: Pay special attention to the __bolded words__ in the definitions above. They will be important in understanding the experiments later on.

## 🍰 Slicing the works ##
Each node stores a `win_count`, `total_simulations` and `score`. Implementation-wise, each node also stores a list of `children` and a reference to its `parent`.

1. Selection
    * Recurse until best scored, leaf node
    * If "ghost child" present, return it.
    * Else If children left to be added, create all children as "ghosts". Then return any "ghost child".
    ![children of the ghosts](https://docs.google.com/uc?id=0B-Vq2G3EGHHGc1Z0Vlp4QkpkWU0)
    > _What am I doing here?_ - The expansion step that follows this uses a sub-function that returns every possible move from a given state.  
    > Consider that __every time__ we want to create a child, we need to do an every pair, parent-child comparison to find which child has and hasn't been created.  
    > As this is expensive, we are instead creating all the children at once and simply having the selection function treat them as if they aren't really there yet. Hence the "ghost child" name.

2. Expansion
    * If "ghost child", convert to plain child node.

3. Simulation
    * Simulate to end game. Report win/lose.

4. Backpropagation
    * Update tree using simulation results.
    
## 👾 How to play ##
_The game requires Python 3. If you don't already have it, get started [here](https://www.python.org/downloads/)._

Want to join the fun? Start by cloning the repository. Then, edit file UTTT/UTTT.py to choose your preferred implementation to fight out.
Change players `agent_x` and `agent_o` to one of `Uct()`, `Decisive()`, `Grave()` and `New()`. Always use the argument `1` for `agent_x` and `-1` for `agent_o`. 

> _Why 1 and -1?_ - The evaluation function that determines if a state is a win state or otherwise depends on the player's markers being these numbers to work.  
> See the method `inner_win()` in UTTT/State.py to understand the magic. 

* For example, to play UCT (as player X, starts first) against GRAVE (as player O): from line 28, in UTTT/UTTT.py

        self.agent_x = Uct(1)
        self.agent_o = Grave(-1)
        
To change the number of iterations at each turn, simply modify the for loop condition which surrounds the `iterate()` function calls.
        
* For example, to give each player 250 iterations "to think" at each turn, from line 37, in UTTT/UTTT.py

        for i in range(250):
    
Once you have set your agents, run UTTT/UTTT.py and give it a minute a two! Results will be printed at the end of the game, along with relevant output information.
If the winner is `1`, X has won. If the winner is `-1`, O has won. If the winner is `0`, then the game has ended in a draw.

* Example output of a game with New(1) vs. Grave(-1)

        winner: -1
                      │               │              
            │ O │ O   │     │   │     │   O │ X │ O
         ───┼───┼───  │  ───┼───┼───  │  ───┼───┼───
          X │ O │ O   │     │   │     │   X │ O │ X
         ───┼───┼───  │  ───┼───┼───  │  ───┼───┼───
          X │ O │ X   │   X │ X │ X   │   X │ O │ X
        ──────────────┼───────────────┼──────────────
            │   │ X   │     │ O │     │     │ O │  
         ───┼───┼───  │  ───┼───┼───  │  ───┼───┼───
          O │ X │ O   │   X │ O │     │     │ X │  
         ───┼───┼───  │  ───┼───┼───  │  ───┼───┼───
          X │ O │ X   │   X │ O │ O   │   X │ X │ X
        ──────────────┼───────────────┼──────────────
            │   │     │   X │ O │ X   │     │   │ O
         ───┼───┼───  │  ───┼───┼───  │  ───┼───┼───
          O │ O │ O   │   O │ O │ X   │     │ X │ O
         ───┼───┼───  │  ───┼───┼───  │  ───┼───┼───
          X │   │     │   O │   │ X   │   O │ X │ O
                      │               │              
        time taken: 33.05485725402832
      
To change an algorithm's parameter values, such as the _c_ value in UCT, the _bias_ and _ref_ values in GRAVE or even the _alpha_ value in New, 
simply access the corresponding <MCTS_variant>/<mcts_variant>_settings.py file and edit the values within.
    
## 👓 Experiments ##

### Tuning Parameters ###

Tuning the _bias_ in GRAVE against UCT at UTTT with 100 trials, 100 iterations each turn:

| _bias_          | 10⁻¹  | 10⁻²  | 10⁻³  | 10⁻⁴  | 10⁻⁵  | 10⁻⁶  | 10⁻⁷  | 10⁻⁸  | 10⁻⁹  | 10⁻¹⁰ | 10⁻¹¹ | __10⁻¹²__ | 10⁻¹³ | 10⁻¹⁴ | 10⁻¹⁵ |
|:--------------- | :---: | :---: | ----: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| GRAVE win rate  | 77.8% | 69.0% | 76.0% | 79.0% | 77.0% | 81.8% | 79.0% | 82.0% | 78.8% | 80.0% | 77.0% | __84.0%__ | 79.0% | 76.0% | 71.0% |

> _What ref value are we using to tune the bias?_ - GRAVE with a _ref_ value of `0` is equivalent to a standard implementation of RAVE and is the value which will be used. 

Tuning the _ref_ in GRAVE against UCT at UTTT with 100 trials, 100 iterations each turn:

| _ref_           | 1     | __5__     | 10    | 20    | 25    | 50    | 100   | 200   | 400   |
|:--------------- | :---: | :-------: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| GRAVE win rate  | 81.0% | __83.0%__ | 80.0% | 71.0% | 68.0% | 50.0% | 61.0% | 50.0% | 49.5% |

> _Why these values?_ - These were the original tuning tests ran in the paper which first proposed GRAVE; except while in the paper the algorithm was used to play Atarigo/Go/Domineering etc., here we are playing UTTT.

> _What about the c value?_ - A value of `0.4` will be used, which is the recommended value used in GGP.

Tuning the _alpha_ in New against UCT at UTTT in 100 trials, 100 iterations each turn:

|_alpha_       | 0.05  | 0.10  | 0.15  | 0.20  | 0.25  | 0.30  | 0.35  | 0.40  | 0.45  | 0.50  | __0.55__  | 0.60  | 0.65  | 0.70  | 0.75  | 0.80  | 0.85  | 0.90  | 0.95  | 
|:------------ | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :-------: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
|New win rate  | 56.0% | 62.0% | 64.0% | 69.0% | 65.0% | 67.0% | 73.0% | 71.0% | 57.0% | 61.0% | __74.0%__ | 67.0% | 66.0% | 63.0% | 61.6% | 65.0% | 66.0% | 71.0% | 71.0% |

> _Why these values?_ - The original paper had little detail on how this parameter was tuned, but claimed that values `0.05` and `0.25` produced pseudo-optimal solutions.  
> Leaving me open for imagination (the value for alpha must be between 0<α<1), I simply figured that testing all possible values in an interval of 0.05 should be sufficient for our purposes.

### General Playing Strength ###

Round robin (read row first i.e. row as first player, row as winner) in 100 trials, 100 iterations each turn:

| Agent         | UCT       | New       | Decisive  | GRAVE     |
|:------------- | :-------: | :-------: | :-------: | :-------: |
| __UCT__       |     -     |   36.0%   |   59.0%   |   25.0%   |
| __New__       |   70.0%   |     -     |   67.0%   |   43.0%   |
| __Decisive__  |   52.0%   |   38.0%   |     -     |   21.0%   |
| __GRAVE__     |   84.0%   |   71.0%   |   87.0%   |     -     |

### Sacrifice-play Strength ###

__All setups will be tested in 100 trials.__ Scores show *rate of sacrifice path play*__/__*rate of sacrifice move play*__.__ 

> _How to read the following sacrifice scenario diagrams?_ - Markers in black/grey indicate presence at the beginning of the sacrifice state. Each scenario begins with X having to play in the lower left inner grid. 
> The complete and correct sacrifice path is marked out using red markers, in the order of the numbers specified beside them (e.g. X₁ is the sacrifice move; and [X₁, O₂, X₃] is the sacrifice path).

Rate of sacrifice play in a terminal, 3-move scenario:

![Terminal-3](https://docs.google.com/uc?id=0B-Vq2G3EGHHGTk9yMzkwLTV6WFE)

| Agent\Iterations  | 100     | 1000    | 10 000  |
|:----------------- | :-----: | :-----: | :-----: |
| __UCT__           |  16/17  |  27/27  |  28/28  |
| __New__           |  23/23  |  54/54  |  91/91  |
| __Decisive__      |   6/7   |  13/13  |  16/16  |
| __GRAVE__         |  21/21  |  14/14  |  22/22  |

Rate of sacrifice play in a terminal, 5-move scenario:

![Terminal-5](https://docs.google.com/uc?id=0B-Vq2G3EGHHGQjVDNVAyczJiLWM)

| Agent\Iterations  | 100     | 1000    | 10 000  |
|:----------------- | :-----: | :-----: | :-----: |
| __UCT__           |   8/10  |  13/14  |   9/12  |
| __New__           |   7/8   |   3/3   |   0/0   |
| __Decisive__      |   6/6   |   5/7   |  10/10  |
| __GRAVE__         |  10/11  |   9/12  |   4/6   |

Rate of sacrifice play in a terminal, 7-move scenario:

![Terminal-7](https://docs.google.com/uc?id=0B-Vq2G3EGHHGdDgzaTBROWFWQk0)

| Agent\Iterations  | 100     | 1000    | 10 000  |
|:----------------- | :-----: | :-----: | :-----: |
| __UCT__           |   4/24  |   5/22  |   3/22  |
| __New__           |   2/12  |   0/2   |   0/2   |
| __Decisive__      |   4/39  |   1/31  |   5/38  |
| __GRAVE__         |   0/20  |   1/15  |   1/13  |

Rate of sacrifice play in a non-terminal, 3-move scenario:

![Non-Terminal-3](https://docs.google.com/uc?id=0B-Vq2G3EGHHGNVVmaFJKWURTTFk)

| Agent\Iterations  | 100     | 1000    | 10 000  |
|:----------------- | :-----: | :-----: | :-----: |
| __UCT__           |  21/24  |  29/30  |  31/32  |
| __New__           |  20/20  |  29/29  |  24/24  |
| __Decisive__      |  18/23  |  32/34  |  21/24  |
| __GRAVE__         |   8/8   |  10/10  |   9/9   |

Rate of sacrifice play in a non-terminal, 5-move scenario:

![Non-Terminal-5](https://docs.google.com/uc?id=0B-Vq2G3EGHHGRlVQdEgyU0lTblk)

| Agent\Iterations  | 100     | 1000    | 10 000  |
|:----------------- | :-----: | :-----: | :-----: |
| __UCT__           |   7/10  |   9/9   |   9/11  |
| __New__           |   4/7   |   2/2   |   0/0   |
| __Decisive__      |   4/9   |   5/5   |   9/10  |
| __GRAVE__         |   6/9   |   9/11  |   5/7   |

Rate of sacrifice play in a non-terminal, 7-move scenario:

![Non-Terminal-7](https://docs.google.com/uc?id=0B-Vq2G3EGHHGYXF1QzRWdDV6Uzg)

| Agent\Iterations  | 100     | 1000    | 10 000  |
|:----------------- | :-----: | :-----: | :-----: |
| __UCT__           |   1/4   |   0/1   |   1/1   |
| __New__           |   1/1   |   0/0   |   0/0   |
| __Decisive__      |   2/8   |   1/2   |   1/1   |
| __GRAVE__         |   0/1   |   0/0   |   0/1   |
